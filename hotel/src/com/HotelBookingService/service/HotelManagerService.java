package com.HotelBookingService.service;

import com.HotelBookingService.model.HotelManager;

import java.util.List;


public interface HotelManagerService {

    public void addHotelManager(HotelManager hm);
    public void update(HotelManager hm);
    public void deleteHotel(HotelManager hm);
    public HotelManager getHotelManagerById(int hmId);
    public List<HotelManager> getAllHotelManagers();
    public HotelManager getHotelManagerByEmail(String email, String pass);
    public boolean isUniqueEmail(String email);
}
