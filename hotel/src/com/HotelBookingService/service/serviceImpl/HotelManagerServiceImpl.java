package com.HotelBookingService.service.serviceImpl;

import com.HotelBookingService.dao.HotelManagerDAO;
import com.HotelBookingService.model.HotelManager;
import com.HotelBookingService.service.HotelManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class HotelManagerServiceImpl implements HotelManagerService {
    @Autowired
    private HotelManagerDAO dao;

    @Transactional
    public void addHotelManager(HotelManager hm) {
        dao.addHotelManager(hm);
    }

    @Transactional
    public void update(HotelManager hm) {
        dao.update(hm);
    }

    @Transactional
    public void deleteHotel(HotelManager hm) {
        dao.delete(hm);
    }

    @Transactional
    public HotelManager getHotelManagerById(int hmId) {
        return dao.getHotelManagerById(hmId);
    }

    @Transactional
    public List<HotelManager> getAllHotelManagers() {
        return dao.getAllHotelManagers();
    }

    @Transactional
    public HotelManager getHotelManagerByEmail(String email, String pass) {
        return dao.getHotelManagerByEmail(email, pass);
    }

    @Transactional
    public boolean isUniqueEmail(String email) {
        return dao.isUniqueEmail(email);
    }

}
