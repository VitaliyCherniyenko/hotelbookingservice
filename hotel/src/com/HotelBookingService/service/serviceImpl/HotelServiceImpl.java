package com.HotelBookingService.service.serviceImpl;

import com.HotelBookingService.dao.HotelDAO;
import com.HotelBookingService.model.Hotel;
import com.HotelBookingService.model.HotelStatus;
import com.HotelBookingService.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class HotelServiceImpl implements HotelService {
    @Autowired
    private HotelDAO dao;

    public HotelDAO getDao() {
        return dao;
    }

    public void setDao(HotelDAO dao) {
        this.dao = dao;
    }

    @Transactional
    public void addHotel(Hotel hotel) {
        dao.addHotel(hotel);
    }

    @Transactional
    public void update(Hotel hotel) {
        dao.update(hotel);
    }

    @Transactional
    public void deleteHotel(Hotel hotel) {
        dao.deleteHotel(hotel);
    }

    @Transactional
    public Hotel getHotelById(long hotelId) {
        return dao.getHotelById(hotelId);
    }

    @Transactional
    public List<Hotel> getAllHotels() {
        return dao.getAllHotels();
    }

    @Transactional
    public List<Hotel> getAllHotelsByName(String hotelName) {
        return dao.getAllHotelsByName(hotelName);
    }

    @Transactional
    public List<Hotel> getAllHotelsByStatus(HotelStatus status) {
        return dao.getAllHotelsByStatus(status);
    }
}
