package com.HotelBookingService.service.serviceImpl;

import com.HotelBookingService.dao.RoomDAO;
import com.HotelBookingService.model.Room;
import com.HotelBookingService.model.RoomStatus;
import com.HotelBookingService.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    private RoomDAO dao;
    @Transactional
    public void addRoom(Room room) {
        dao.addRoom(room);
    }

    @Transactional
    public void update(Room room) {
        dao.update(room);
    }

    @Transactional
    public void deleteRoom(Room room) {
        dao.deleteRoom(room);
    }

    @Transactional
    public Room getRoomById(int roomId) {
        return dao.getRoomById(roomId);
    }

    @Transactional
    public List<Room> getAllRooms() {
        return dao.getAllRooms();
    }

    @Override
    public List<Room> getAllRoomsByPrice(int price) {
        return dao.getAllRoomsByPrice(price);
    }

    @Override
    public List<Room> getAllRoomsByStatus(RoomStatus status) {
        return dao.getAllRoomsByStatus(status);
    }
}
