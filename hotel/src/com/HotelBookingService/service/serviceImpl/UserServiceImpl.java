package com.HotelBookingService.service.serviceImpl;

import com.HotelBookingService.dao.UserDAO;
import com.HotelBookingService.model.User;
import com.HotelBookingService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO dao;
    @Transactional
    public void addUser(User user) {
        dao.addUser(user);
    }

    @Transactional
    public void update(User user) {
       dao.update(user);
    }

    @Transactional
    public void deleteUser(User user) {
        dao.deleteUser(user);
    }

    @Transactional
    public User getUserById(int userId) {
        return dao.getUserById(userId);
    }

    @Transactional
    public List<User> getAllUsers() {
        return dao.getAllUsers();
    }

    @Transactional
    public User getUserByEmail(String email, String pass) {
        return dao.getUserByEmail(email,pass) ;
    }

    @Transactional
    public boolean isUniqueEmail(String eMail) {
        return dao.isUniqueEmail(eMail);
    }


}
