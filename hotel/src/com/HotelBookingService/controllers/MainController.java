package com.HotelBookingService.controllers;

import com.HotelBookingService.model.Hotel;
import com.HotelBookingService.model.HotelManager;
import com.HotelBookingService.model.User;
import com.HotelBookingService.service.HotelManagerService;
import com.HotelBookingService.service.HotelService;
import com.HotelBookingService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    private HotelManagerService hms;
    @Autowired
    private UserService userService;
    @Autowired
    private HotelService hotelService;


    @RequestMapping("/regForm")
    public String regForm() {
        return "regForm";
    }

    @RequestMapping("/hotel/{id}")
    public  String showHotel(@PathVariable int id, Model map){
        map.addAttribute("hotel", hotelService.getHotelById(id));
      return "showHotel";
    }

    @RequestMapping(value = "/registerHM", method = RequestMethod.POST)
    public
    @ResponseBody
    HotelManager regHm(@RequestParam String firstName, @RequestParam String sureName,
                       @RequestParam int age, @RequestParam String email,
                       @RequestParam String pass,
                       @RequestParam MultipartFile photo, Model map) throws IOException {

        if (!hms.isUniqueEmail(email)) {
            return new HotelManager();
        } else {
        HotelManager manager = new HotelManager();
        manager.setPhoto(photo.getBytes());
        manager.setFirstName(firstName);
        manager.setSureName(sureName);
        manager.setAge(age);
        manager.seteMail(email);
        manager.setPass(pass);
        hms.addHotelManager(manager);
        return manager; }
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = "application/json")
    public
    @ResponseBody
    User regUser(@RequestParam String firstName, @RequestParam String sureName,
                          @RequestParam int age, @RequestParam String email,
                          @RequestParam String pass,
                          @RequestParam MultipartFile photo, Model map) throws IOException {
        if (!userService.isUniqueEmail(email)) {
            return new User();
        } else {
        User user = new User();
        user.setPhoto(photo.getBytes());
        user.setFirstName(firstName);
        user.setSureName(sureName);
        user.setAge(age);
        user.seteMail(email);
        user.setPass(pass);
        userService.addUser(user);

        return  user; }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public
    @ResponseBody
    User logIn(@RequestParam String email, @RequestParam String pass) {
        return userService.getUserByEmail(email, pass);
    }

    @RequestMapping(value = "/loginHM", method = RequestMethod.POST)
    public
    @ResponseBody
    HotelManager logInHM(@RequestParam String email, @RequestParam String pass) {
        return hms.getHotelManagerByEmail(email, pass);
    }

    @RequestMapping(value = "/autoCompleteSearchHotelsByName", method = RequestMethod.POST)
    public
    @ResponseBody
    List<Hotel> autoCompleteSearchHotels() {
        List<Hotel> list = hotelService.getAllHotels();
        return list;
    }

    @RequestMapping("/index")
    public String index(Model map) {
        map.addAttribute("myHotel", "message");
        return "index";
    }

    @RequestMapping("/hotelManager")
    public String hotelManagerForm() {
        return "hotelManager";
    }

    @RequestMapping("/addHotelForm")
    public String addHotel() {
        return "addHotelForm";
    }

    @RequestMapping(value = "/search")
    public String searchFrom() {
        return "searchForm";
    }

    @RequestMapping(value = "/addHotel", headers = "content-type=multipart/*", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Hotel addHotel(@RequestParam String name, @RequestParam String site, @RequestParam String city,
                   @RequestParam String description,
                   @RequestParam MultipartFile photo,
                   @RequestParam String status, Model map) throws IOException {
        Hotel hotel = new Hotel();
        hotel.setName(name);
        hotel.setPhoto(photo.getBytes());
        hotelService.addHotel(hotel);
        return hotel;
    }

    @RequestMapping(value = "/searchHotelByName", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<Hotel> test(@RequestParam String name) {
        return hotelService.getAllHotelsByName(name);
    }

    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    public String hello(@RequestParam("photo") MultipartFile file, Model map) {
        Hotel hotel = new Hotel();
        try {
            hotel.setPhoto(file.getBytes());
            hotelService.addHotel(hotel);
            System.out.println(hotel.getId());
            hotel.setName(file.getOriginalFilename());
        } catch (IOException e) {
            e.printStackTrace();
        }
        map.addAttribute("hotel", hotel);
        map.addAttribute("message", "helloWorld");
        return "hello";
    }

    @RequestMapping(value = "/imageController/{imgId}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] helloWorld(@PathVariable long imgId) {
        return hotelService.getHotelById(imgId).getPhoto();
    }
}
