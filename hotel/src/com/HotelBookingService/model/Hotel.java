package com.HotelBookingService.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Hotel {
    @Id
    @GeneratedValue
    private long id;
    @Enumerated
    private HotelStatus status;
    private String name;
    private String address;
    private String site;
    private String description;
    @OneToMany
    private List<Order> orders = new ArrayList<Order>();
    @OneToOne
    private HotelManager manager;
    @Lob
    private byte [] photo;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST, mappedBy = "hotel")
    private List<Room> rooms = new ArrayList<Room>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public HotelStatus getStatus() {
        return status;
    }

    public void setStatus(HotelStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HotelManager getManager() {
        return manager;
    }

    public void setManager(HotelManager manager) {
        this.manager = manager;
    }

    public byte [] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }
}
