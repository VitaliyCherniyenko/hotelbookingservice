package com.HotelBookingService.model;

import javax.persistence.*;

/**
 * Created by Patriot on 17.04.14.
 */
@Entity
public class Order {
    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Hotel hotel;
}
