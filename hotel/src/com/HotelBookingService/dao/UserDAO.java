package com.HotelBookingService.dao;

import com.HotelBookingService.model.User;

import java.util.List;


public interface UserDAO {

    public void addUser(User user);
    public void update(User user);
    public void deleteUser(User user);
    public User getUserById(int userId);
    public List<User> getAllUsers();
    public User getUserByEmail(String email, String pass);
    public boolean isUniqueEmail(String eMail);


}
