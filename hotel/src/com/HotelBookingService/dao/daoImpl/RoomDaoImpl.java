package com.HotelBookingService.dao.daoImpl;

import com.HotelBookingService.dao.RoomDAO;
import com.HotelBookingService.model.Room;
import com.HotelBookingService.model.RoomStatus;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class RoomDaoImpl implements RoomDAO {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public void addRoom(Room room) {
        sessionFactory.getCurrentSession().save(room);
    }

    @Override
    public void update(Room room) {
        sessionFactory.getCurrentSession().update(room);
    }

    @Override
    public void deleteRoom(Room room) {
        sessionFactory.getCurrentSession().delete(room);
    }

    @Override
    public Room getRoomById(int roomId) {
        return (Room) sessionFactory.getCurrentSession().get(Room.class, roomId);
    }

    @Override
    public List<Room> getAllRooms() {
        return sessionFactory.getCurrentSession().createCriteria(Room.class).list();
    }

    @Override
    public List<Room> getAllRoomsByPrice(int price) {
        return sessionFactory.getCurrentSession().createCriteria(Room.class)
                .add(Restrictions.between("price", price-50 ,price+50)).list();
    }

    @Override
    public List<Room> getAllRoomsByStatus(RoomStatus status) {
        return sessionFactory.getCurrentSession().createCriteria(Room.class)
                .add(Restrictions.eq("status",status)).list();
    }
}
