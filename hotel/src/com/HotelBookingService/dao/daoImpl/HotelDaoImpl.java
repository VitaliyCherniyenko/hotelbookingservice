package com.HotelBookingService.dao.daoImpl;

import com.HotelBookingService.dao.HotelDAO;
import com.HotelBookingService.model.Hotel;
import com.HotelBookingService.model.HotelStatus;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HotelDaoImpl implements HotelDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addHotel(Hotel hotel) {
        sessionFactory.getCurrentSession().save(hotel);
    }

    @Override
    public void update(Hotel hotel) {
        sessionFactory.getCurrentSession().update(hotel);
    }

    @Override
    public void deleteHotel(Hotel hotel) {
        sessionFactory.getCurrentSession().delete(hotel);
    }

    @Override
    public Hotel getHotelById(long hotelId) {
        return (Hotel)sessionFactory.getCurrentSession().get(Hotel.class, hotelId);
    }

    @Override
    public List<Hotel> getAllHotels() {
        return sessionFactory.getCurrentSession().createCriteria(Hotel.class).list();
    }

    @Override
    public List<Hotel> getAllHotelsByName(String hotelName) {
        return sessionFactory.getCurrentSession().createCriteria(Hotel.class)
                .add(Restrictions.like("name", hotelName)).list();
    }

    @Override
    public List<Hotel> getAllHotelsByStatus(HotelStatus status) {
        return sessionFactory.getCurrentSession().createCriteria(Hotel.class)
                .add(Restrictions.eq("status",status)).list();
    }
}
