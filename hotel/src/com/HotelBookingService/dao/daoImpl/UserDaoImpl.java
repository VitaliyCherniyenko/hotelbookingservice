package com.HotelBookingService.dao.daoImpl;

import com.HotelBookingService.dao.UserDAO;
import com.HotelBookingService.model.User;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public void update(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }

    @Override
    public User getUserById(int userId) {
        return (User) sessionFactory.getCurrentSession().get(User.class, userId);
    }

    @Override
    public List<User> getAllUsers() {
        return sessionFactory.getCurrentSession().createCriteria(User.class).list();
    }

    @Override
    public User getUserByEmail(String email, String pass) {
        List<User> list = sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.like("eMail", email)).add(Restrictions.eq("pass",pass)).list();
        if (list.size() > 0) return list.get(0);
        else return new User();
    }

    public boolean isUniqueEmail(String eMail) {
        List res = sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.like("eMail", eMail)).list();
        if (res.size() > 0) {
            return false;
        } else {
            return true;
        }
    }


}
