package com.HotelBookingService.dao.daoImpl;

import com.HotelBookingService.dao.HotelManagerDAO;
import com.HotelBookingService.model.HotelManager;
import com.HotelBookingService.model.User;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HotelManagerDaoImpl implements HotelManagerDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addHotelManager(HotelManager hm) {
        sessionFactory.getCurrentSession().save(hm);
    }

    @Override
    public void update(HotelManager hm) {
        sessionFactory.getCurrentSession().update(hm);
    }

    @Override
    public void delete(HotelManager hm) {
        sessionFactory.getCurrentSession().delete(hm);
    }

    @Override
    public HotelManager getHotelManagerById(int hmId) {
        return (HotelManager) sessionFactory.getCurrentSession().get(HotelManager.class, hmId);
    }

    @Override
    public List<HotelManager> getAllHotelManagers() {
        return sessionFactory.getCurrentSession().createCriteria(HotelManager.class).list();
    }

    @Override
    public HotelManager getHotelManagerByEmail(String email, String pass) {
        List<HotelManager> list = sessionFactory.getCurrentSession().createCriteria(HotelManager.class)
                .add(Restrictions.like("eMail", email)).add(Restrictions.eq("pass", pass)).list();
        if(list.size()>0) return list.get(0);
        else return new HotelManager();
    }

    @Override
    public boolean isUniqueEmail(String email) {
        List<HotelManager> list = sessionFactory.getCurrentSession().createCriteria(HotelManager.class)
                .add(Restrictions.like("eMail",email)).list();
        if (list.size() > 0) {
            return false;
        } else {
            return true;
        }
    }


}
