package com.HotelBookingService.dao;

import com.HotelBookingService.model.Hotel;
import com.HotelBookingService.model.HotelStatus;

import java.util.List;


public interface HotelDAO {

    public void addHotel(Hotel hotel);
    public void update(Hotel hotel);
    public void deleteHotel(Hotel hotel);
    public Hotel getHotelById(long hotelId);
    public List<Hotel> getAllHotels();
    List<Hotel> getAllHotelsByName(String hotelName);
    public List<Hotel> getAllHotelsByStatus(HotelStatus status);

}
