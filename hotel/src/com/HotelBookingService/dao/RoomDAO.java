package com.HotelBookingService.dao;

import com.HotelBookingService.model.Room;
import com.HotelBookingService.model.RoomStatus;

import java.util.List;


public interface RoomDAO {

    public void addRoom(Room room);
    public void update(Room room);
    public void deleteRoom(Room room);
    public Room getRoomById(int roomId);
    public List<Room> getAllRooms();
    public List<Room> getAllRoomsByPrice(int price);
    public List<Room> getAllRoomsByStatus(RoomStatus status);


}
