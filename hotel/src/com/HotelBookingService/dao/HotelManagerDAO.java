package com.HotelBookingService.dao;

import com.HotelBookingService.model.HotelManager;

import java.util.List;


public interface HotelManagerDAO {

    public void addHotelManager(HotelManager hm);
    public void update(HotelManager hm);
    public void delete(HotelManager hm);
    public HotelManager getHotelManagerById(int hmId);
    public List<HotelManager> getAllHotelManagers();
    public HotelManager getHotelManagerByEmail(String email, String pass);
    public boolean isUniqueEmail(String email);
}
