<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register Form</title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="/resources/jquery.form.js"></script>
</head>
<body>
<h2>Register Form</h2>
<script>
    $(document).ready(function () {
                $('#isManager').change(function () {
                    if ($("#isManager").is(":checked")) {
                        $('form').get(0).setAttribute('action', '/registerHM.form');
                    } else {
                        $('form').get(0).setAttribute('action', '/registerUser.form');
                    }
                })

                $('#regForm').ajaxForm({
                    dataType: 'json',
                    success: function (data) {
                        if (data.id !== 0) {
                            alert('saved with name ' + data.firstName);
                            window.location.href = '/index.form';
                        }
                        else alert('fail')
                    }
                })
            }
    )
</script>
<form id="regForm" method="post" action="/registerUser.form" enctype="multipart/form-data">
    <fieldset>
        <p>
            <label for="firstName">firstName (required, at least 2 characters)</label>
            <input id="firstName" name="firstName" minlength="2" type="text" required/>
        </p>

        <p>
            <label for="sureName">last name</label>
            <input id="sureName" type="text" name="sureName" minlength="2" required/>
        </p>

        <p>
            <label for="age">age</label>
            <input id="age" type="text" name="age"/>
        </p>

        <p>
            <label for="email">email</label>
            <input id="email" type="email" name="email" required/>
        </p>

        <p>
            <label for="pass">pass</label>
            <input id="pass" type="password" name="pass" minlength="4" required/>
        </p>

        <p>
            <label for="photo">Photo </label>
            <input type="file" id="photo" name="photo"/>
        </p>

        <p>
            <label for="isManager">is Manager hotel </label>
            <input type="CHECKBOX" id="isManager" name="manager"/>
        </p>

        <p>
            <input type="submit" id="button" value="Submit"/>
        </p>
    </fieldset>
</form>

</body>
</html>
