
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><h2>Add hotel</h2></title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="/resources/jquery.form.js"></script>
</head>
<body>
<script>
    $(document).ready(function(){
            $('#hotelForm').ajaxForm(
                    {
                        dataType:'json',
                        success:function(data){
                            alert('saved with name'+ data.name)
                            window.location.href = '/index.form';
                        }
                    }
            )}
    )
</script>
<form id="hotelForm" method="post" action="/addHotel.form" enctype="multipart/form-data">
    <fieldset>
        <p>
            <label for="hotelName">hotelName (required, at least 2 characters)</label>
            <input id="hotelName" name="name" minlength="2" type="text" required/>
        </p>

        <p>
            <label for="site">Site</label>
            <input id="site" type="url" name="site"/>
        </p>
        <p>
            <label for="city">City</label>
            <input type="text" id="city" name="city" required/>
        </p>

        <p>
            <label for="description">Your comment (required)</label>
            <textarea id="description" name="description" required></textarea>
        </p>

        <p>
            <label for="photo">Photo </label>
            <input type="file" id="photo" name="photo"></textarea>
        </p>

        <p>
            <label for="status">Status </label>
            <select id="status" name="status">
                <option label="*" value="D"/>
                <option label="**" value="C"/>
                <option label="***" value="B"/>
                <option label="****" value="A"/>
                <option label="*****" value="DeLux"/>
            </select>
        </p>
        <p>
            <input type="submit" id="button" value="Submit"/>
        </p>
    </fieldset>
</form>

</body>
</html>
