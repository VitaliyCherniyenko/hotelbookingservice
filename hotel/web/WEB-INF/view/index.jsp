<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <style type="text/css">
        #logIn {
            width: 200;
            margin-bottom: 5px;
            padding: 5px;
            border: solid #080309;
            cursor: pointer;
        }
        #form {
            width: 200;
            margin-bottom: 5px;
            padding: 5px;
            border: solid #080309;
        }
    </style>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="/resources/jquery.form.js"></script>
    <script type="text/javascript" src="/resources/jquery.cookie.js"></script>
    <script type="text/javascript" src="/resources/jquery-ui-1.9.2.custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if ($.cookie('id')) {
                $('#logIn').text($.cookie('userName') +' logOut');
                $('#loginForm').hide()
                $('#regLink').hide()
            }
            $('#loginForm').ajaxForm(
                    {
                        dataType: 'json',
                        success: function (data) {
                            if (data.id !== 0) {
                                alert('hello ' + data.firstName)
                                $('#form').hide();
                                $.cookie('userName', data.firstName, {path: "/"});
                                $.cookie('id', data.id, {path: "/"});
                                $('#logIn').append($.cookie('userName') + '<a onclick="">logOut</a>');
                                $('#logIn').hide;
                            }
                            else alert('fail')
                        }
                    }
            );
            $('#logIn').click(
                    function () {
                        if ($.cookie('id')) {
                            $.cookie('userName', null, {path: "/"});
                            $.cookie('id', null, {path: "/"});
                            $.cookie('manager', null)
                            $('#logIn').text('logIn');
                            $('#loginForm').show()
                            $('#regLink').hide()
                            $('#hotelManager').hide()
                        }
                    }
            )
            $('#manager').change(function(){
                if ($("#manager").is(":checked")) {
                    $.cookie('manager', true)
                    $('form').get(0).setAttribute('action', '/loginHM.form');
                } else {
                    $.cookie('manager', false)
                    $('form').get(0).setAttribute('action', '/login.form');
                }
            })
        })
    </script>
    <title>Welcome Page</title>
</head>

<div class="auth" id="logIn">
    login
</div>
<div id="form">
    <form id="loginForm" method="post" action="/login.form">
        email<input type="email" name="email" id="email" required><br/>
        pass <input type="password" id="pass" name="pass" required><br/>
        <input type="submit" onclick="" value="register"/>
        manager <input type="checkbox" id="manager"/>
    </form>
</div>
<body>
<a href="/search.form">Serch hotel</a> <br/>
<a href="/hotelManager.form" id="hotelManager"> Hotel Manager</a><br/>
<a id="regLink" href="/regForm.form"> регистрация</a>
<br/>
</body>
</html>
