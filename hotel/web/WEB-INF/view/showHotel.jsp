<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${hotel.name}</title>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="/resources/jquery.form.js"></script>
    <script type="text/javascript" src="/resources/jquery.cookie.js"></script>
    <script type="text/javascript" src="/resources/jquery-ui-1.9.2.custom.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (!$.cookie('id')) {
            alert('you are nou login')}
            $('#order').click(function(){
                type: 'json',
                $.ajax({
                    url:'/addOrder',
                    data:{'manager': $.cookie('manager'), 'hotelId':${hotel.id}, 'userId':$.cookie('id')},
                    success: function(){
                        alert('added')
                    }
                })


            })
        })
    </script>
</head>
<body>
<h2>Description</h2>

<p>${hotel.description}</p><br/>
<table>
    <tr>
        <td valign="top">
            <p>city: ${hotel.address}</p>

            <p>status: ${hotel.status}</p>

            <p><a id="order" href="#">order room</a></p>
        </td>
        <td><img src="/imageController/${hotel.id}.form"/></td>
    </tr>
</table>

</body>
</html>
