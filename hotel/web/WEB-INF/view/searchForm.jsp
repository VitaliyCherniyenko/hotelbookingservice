<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/resources/jquery.form.js"></script>
    <script type="text/javascript" src="/resources/jquery-ui-1.9.2.custom.js"></script>
</head>

<body>
<form id="form" method="get" action="/searchHotelByName.form">
    <input type="text" id="search" name="name">
    <input type="submit" id="button" value="button"/>
</form>
<div id="tab"></div>
<script>
    $(document).ready(function () {
        $('#search').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/autoCompleteSearchHotelsByName.form",
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.name,
                                value: item.name
                            }
                        }));
                    },
                    error: function (error) {
                        alert('error: ' + error);
                    }
                });
            }
//            minLength: 2
        });

        $('#form').ajaxForm(
                {
                    dataType: 'json',
                    success: function (data) {
                        var innerCode='<table>';
                        for(var i=0; i<data.length; i++){
                            var hotel = data[i];
                            var img='<img src="/imageController/'+hotel.id+'.form" width="200" height="200"/>'
                            var url='<a href="/hotel/'+ hotel.id+'.form">'+hotel.name+' </a>'
                            innerCode=innerCode+'<tr><td>'+img+'</td><td>'+url+'</td></tr>'
                        }
                        innerCode+='</table>';
                        $('#tab').append(innerCode)
                    }})
    });

</script>
</body>
</html>
